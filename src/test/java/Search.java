import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Search {
    WebDriver driver;
    By search = By.id("query");
    By confirm = By.id("search");
    By title = By.className("title");
    public Search(WebDriver driver) {
        this.driver = driver;
    }
    public void run(String name, String doi, String date){

        try {
            Thread.sleep(1000);//WebDriverWait wait didnt work
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(search).sendKeys(name);
        driver.findElement(confirm).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<WebElement> elements = driver.findElements(title);
        //System.out.println(elements.size());
        SaveData saveData = new SaveData();
        Article article1 =new Article(driver, saveData);
        try {
            article1.run(elements.get(0));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] found = (saveData.getData()).get(0);
        Assertions.assertEquals(name, found[0]);
        Assertions.assertEquals(doi, found[1]);
        Assertions.assertEquals(date, found[2]);
        driver.navigate().back();
    }

}

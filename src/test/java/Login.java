import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login {
    WebDriver driver;
    By email = By.id("login-box-email");
    By password = By.id("login-box-pw");
    By confirm = By.className("form-submit");

    public Login(WebDriver driver) {
        this.driver = driver;
    }

    public void log(String name, String password){
        driver.get("https://link.springer.com/signup-login ");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("onetrust-accept-btn-handler"))).click();
        driver.findElement(email).sendKeys(name);
        driver.findElement(this.password).sendKeys(password);
        driver.findElement(confirm).findElement(By.tagName("button")).click();
    }
}

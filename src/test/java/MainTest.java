import org.junit.jupiter.api.Test;
//import org.junit.jupiter.params.ParameterizedTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class MainTest {

    @Test
    public void test(){
        SaveData saveData = new SaveData();
        try {
            saveData= (new Selenium()).run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.setProperty("webdriver.edge.driver", "C:\\Users\\pc\\Downloads\\edgedriver_win64\\msedgedriver.exe");
        WebDriver driver = new EdgeDriver();
        Login login = new Login(driver);
        login.log("kubeckat@fel.cvut.cz","Selenium1");
        Search search = new Search(driver);
        for (String[] s:saveData.getData()) {
            search.run(s[0], s[1], s[2]);
            ((JavascriptExecutor)driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

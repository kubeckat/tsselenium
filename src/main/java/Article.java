import org.openqa.selenium.*;

public class Article {
    private final WebDriver driver;
    private final SaveData saveData;


    public Article(WebDriver driver, SaveData saveData) {
        this.driver = driver;
        this.saveData = saveData;

    }
    public void run(WebElement element) throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
        String name = element.getText();
        element.click();

        //WebDriverWait wait = new WebDriverWait(driver, 10);
        //wait.until(ExpectedConditions.elementToBeClickable(By.id("doi-url")));

        Thread.sleep(3000);
        WebElement u;
try {
     u = driver.findElement(By.id("doi-url"));
}catch (NoSuchElementException e){
     u = driver.findElement(By.className("c-bibliographic-information__value"));
}

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", u);
        Thread.sleep(500);

        String doi = u.getText();

        String time;
        try {
            time = driver.findElement(By.className("article-dates__first-online")).getText();
        }catch (NoSuchElementException e){
            time = driver.findElement(By.className("c-bibliographic-information__value")).getText();
        }

        saveData.addData(name, doi, time);
        driver.navigate().back();
    }
}

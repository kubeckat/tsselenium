import java.util.LinkedList;

public class SaveData {
    LinkedList<String[]> data;

    public SaveData() {
        this.data = new LinkedList<>();
    }

    public LinkedList<String[]> getData() {
        return data;
    }

    public void addData(String name, String DOI, String datum) {
        String[] put = {name, DOI, datum};
        this.data.add(put);
    }
}

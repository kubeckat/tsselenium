import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Selenium {

    /*Připravte Page Object Model pro stránky:
https://link.springer.com/    -k přesunu na přihlašovací obrazovku a pokročilé vyhledávání
https://link.springer.com/signup-login    -k přihlášení
https://link.springer.com/advanced-search     -pro pokročilé vyhledávání, tedy práci s formulářem
https://link.springer.com/search    -pro procházení výsledků vyhledávání
https://link.springer.com/article/     -pro čtení článku
Připravte program, který vyhledá na link.springer.com všechny články(Content type = Article) obsahující slova
 “Page Object Model” a alespoň jedním ze slov “Sellenium Testing” publikovaných v tomto roce.
  Poté přečte první čtyři články a uložte si jejich Název, DOI a datum publikace. Napište parametrizované testy
   pomocí vytvořených Page Object Modelů, které Vás přihlásí jako uživatele
 a vyhledávají uložené články (pomocí jejich názvu) a zkontrolují, že odpovídá jejich datum publikace a DOI.*/

    public SaveData run() throws InterruptedException {
        System.setProperty("webdriver.edge.driver", "C:\\Users\\pc\\Downloads\\edgedriver_win64\\msedgedriver.exe");
        WebDriver driver = new EdgeDriver();
        SaveData saveData = new SaveData();
        driver.get("https://link.springer.com/");

        /*WebElement search = driver.findElement(By.id("query"));
        WebElement confirmsearch = driver.findElement(By.id("search"));
        search.sendKeys("Page Object Model");
        confirmsearch.click();*/
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("onetrust-accept-btn-handler"))).click();
        driver.findElement(By.id("search-options")).click();
        driver.findElement(By.id("advanced-search-link")).click();
        AdvancedSearch a = new AdvancedSearch(driver);
        a.fillOut("Object model", "Sellenium Testing", "2021", "2021");
        Read r = new Read(driver, saveData);
        r.run();
        driver.close();
        return  saveData;

    }


}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import java.util.List;

public class Read {
    WebDriver driver;
    By resultlist= By.id("results-list");
    SaveData saveData;
    Article article;

    public Read(WebDriver driver, SaveData saveData) {
        this.driver = driver;
        this.saveData = saveData;

    }
    public void run() throws InterruptedException {

        for(int i = 0; i<4; i++){
            Thread.sleep(2000);
            List<WebElement> elements = driver.findElements(By.className("title"));
            Article article1 =new Article(driver, saveData);
            article1.run(elements.get(i));

        }

    }
}

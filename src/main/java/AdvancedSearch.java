import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvancedSearch {
    WebDriver driver;

    By allWords = By.id("all-words");
    By leastWords = By.id("least-words");
    By title = By.id("title-is");
    By fromYear = By.id("facet-start-year");
    By toYear = By.id("facet-end-year");
    By confirm = By.id("submit-advanced-search");

    public AdvancedSearch(WebDriver driver) {
        this.driver = driver;
    }
    public void fillOut(String allWords, String leastWords, String fromYear, String toYear){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(this.allWords)).sendKeys(allWords);
//driver.findElement(this.allWords).sendKeys(allWords);
driver.findElement(this.leastWords).sendKeys(leastWords);
driver.findElement(this.fromYear).sendKeys(fromYear);
driver.findElement(this.toYear).sendKeys(toYear);
        JavascriptExecutor js = (JavascriptExecutor) driver;
 js.executeScript("window.scrollBy(0,350)", "");


driver.findElement(confirm).click();
    }
}
